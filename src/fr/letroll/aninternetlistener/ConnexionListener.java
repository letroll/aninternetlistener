package fr.letroll.aninternetlistener;

public interface ConnexionListener {
	/**
	 * to use with NetworkStateReceiver
	 */

	/** type = mobile or wifi **/
	public abstract void onConnexionChange(String type);

	public abstract void onConnexionShutdown();

	public abstract void onConnexion(String type);
}