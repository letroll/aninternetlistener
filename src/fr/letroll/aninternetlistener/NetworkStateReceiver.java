package fr.letroll.aninternetlistener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateReceiver extends BroadcastReceiver {
	private String type;
	private ConnexionListener connexionListener = null;
	private boolean connecting = false;

	/*
	 * networkIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"); plus in resume and pause register and unregister
	 */

	public static IntentFilter getFilter() {
		return new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
	}

	public NetworkStateReceiver(ConnexionListener mconnexionListener) {
		connexionListener = mconnexionListener;
	}

	public NetworkStateReceiver() {}

	public void onReceive(Context context, Intent intent) {
		if (intent.getExtras() != null) {
			@SuppressWarnings("deprecation")
			NetworkInfo ni = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
			if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
				type = ni.getTypeName();
				if (connexionListener != null) {
					connexionListener.onConnexionChange(type);
					connecting = true;
				}
				while (connecting) {
					if (isNetworkAvailable())
						if (connexionListener != null) {
							connexionListener.onConnexion(type);
							connecting = false;
						}
				}
			}
		}
		if (intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
			if (connexionListener != null) {
				connexionListener.onConnexionShutdown();
				connecting = false;
			}
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) ((Context) connexionListener).getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	// SIGNAL_STRENGTH_NONE_OR_UNKNOWN (99)
	// SIGNAL_STRENGTH_GREAT (16-32)
	// SIGNAL_STRENGTH_GOOD (8-15)
	// SIGNAL_STRENGTH_MODERATE (4-7)
	// SIGNAL_STRENGTH_POOR (0-3)

	// /**
	// * @throws uses
	// * -permission android:name="android.permission.ACCESS_WIFI_STATE"/>
	// * @param context
	// * @return wifi signal Strengh
	// */
	// public static int getWifiSignalStrength(Context context) {
	// int MIN_RSSI = -100;
	// int levels = 101;
	// WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	// WifiInfo info = wifi.getConnectionInfo();
	// int rssi = info.getRssi();
	//
	// if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
	// return WifiManager.calculateSignalLevel(info.getRssi(), levels);
	// } else {
	// // this is the code since 4.0.1
	// if (rssi <= MIN_RSSI) {
	// return 0;
	// } else
	// return Math.abs(rssi);
	// }
	// }

	// start the signal strength listener
	// SignalStrengthListener signalStrengthListener = new SignalStrengthListener();
	// ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).listen(signalStrengthListener, SignalStrengthListener.LISTEN_SIGNAL_STRENGTHS);
	// ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).listen(signalStrengthListener ,PhoneStateListener.LISTEN_NONE);
	// private class SignalStrengthListener extends PhoneStateListener {
	// @Override
	// public void onSignalStrengthsChanged(android.telephony.SignalStrength signalStrength) {
	//
	// // get the signal strength (a value between 0 and 31)
	// int strengthAmplitude = signalStrength.getGsmSignalStrength();
	//
	// // do something with it (in this case we update a text view)
	// // tv.setText(String.valueOf(strengthAmplitude));
	//
	// tv.setText(String.valueOf(2 * strengthAmplitude - 113));
	// super.onSignalStrengthsChanged(signalStrength);
	// }
	// }
}