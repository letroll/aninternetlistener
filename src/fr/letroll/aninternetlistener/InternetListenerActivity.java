package fr.letroll.aninternetlistener;

import android.content.IntentFilter;
import android.os.Bundle;

import com.magic.activity.MagicActivity;

//<uses-permission android:name="android.permission.ACCESS_NETWORK_S">
public class InternetListenerActivity extends MagicActivity implements ConnexionListener {
	private IntentFilter filter;
	private NetworkStateReceiver receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		receiver = new NetworkStateReceiver(this);
		filter = NetworkStateReceiver.getFilter();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		registerReceiver(receiver, filter);
		super.onResume();
	}

	@Override
	public void onConnexionChange(String type) {}

	@Override
	public void onConnexionShutdown() {}

	@Override
	public void onConnexion(String type) {}
}
